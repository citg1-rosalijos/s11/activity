package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    // Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    // Constructors
    // required when retrieving data from the database
    public Post() {}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
